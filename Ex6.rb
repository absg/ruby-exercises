# A possible interview question from "Effective Programming More Than Writing Code".
# Format an RGB value (three 1-byte numbers) as a 6-digit hexadecimal string.

class Ex6	
	def rgb_to_hexa_m1(n1, n2, n3)
		n1.to_s(16).upcase + n2.to_s(16).upcase + n3.to_s(16).upcase
	end
end

require 'test/unit'

class TestEx6 < Test::Unit::TestCase
	def test_rgb_to_hexa_m1
		ex = Ex6.new
		assert_equal ex.rgb_to_hexa_m1(38, 122, 169), "267AA9"
	end
end