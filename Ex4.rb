# A possible interview question from "Effective Programming More Than Writing Code".
# Write a function that sums up integers from a text file, one int per line. 

class Ex4
	def sum(file)
		res = 0
		
		File.open(file).each_line do |l|
			res += l.to_i
		end
		res
	end
end

require 'test/unit'

class TestEx4 < Test::Unit::TestCase
	def test_sum
		ex = Ex4.new
		assert_equal ex.sum("Ex4_a.txt"), 10

		ex = Ex4.new
		assert_equal ex.sum("Ex4_b.txt"), 20
		
		ex = Ex4.new
		assert_equal ex.sum("Ex4_c.txt"), 1060
	end
end
