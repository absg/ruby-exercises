# A possible interview question from "Effective Programming More Than Writing Code".
# Print out the grade-school multiplication table up to 12 x 12.

def print_mult_tab(x, y)
  (1..x).each do |z|
    (1..y).each do |i|
      puts "#{z} * #{i} = #{z * i}"
    end
    puts
  end
end

print_mult_tab(12, 12)

