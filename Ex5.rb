# A possible interview question from "Effective Programming More Than Writing Code".
# Write a function to reverse a string.

class Ex5	
	def reverse_str(str)
		reversed, i = "", str.length - 1
		
		while i >= 0
			reversed += str[i]
			i -= 1
		end
		reversed
	end
end

require 'test/unit'

class TestEx5 < Test::Unit::TestCase
	def test_reverse_str
		ex = Ex5.new
		assert_equal ex.reverse_str("ola"), "alo"
		assert_equal ex.reverse_str(""), ""
		assert_equal ex.reverse_str("h"), "h"
		assert_equal ex.reverse_str("abcdef"), "fedcba"
	end
end