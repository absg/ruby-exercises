# A possible interview question from "Effective Programming More Than Writing Code".
# Find the largest int value in an int array.

class Ex2
	def max_int(arr)
		max_val = arr[0]

		arr.each do |i|
			max_val = i if i > max_val
		end

		max_val
	end
end

require 'test/unit'
class TestEx2 < Test::Unit::TestCase
  def test_max_int
  	ex = Ex2.new

  	expected = ex.max_int [1, 2, 3, 4, 5]
  	assert_equal expected, 5

  	expected = ex.max_int [11, 200, 5]
  	assert_equal expected, 200

  	expected = ex.max_int [-20, -35, -11, -3, -5]
  	assert_equal expected, -3
  end
end