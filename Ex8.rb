# A possible interview question from "Effective Programming More Than Writing Code".
# Write a program that receives as input a list of strings and returns a list containing the duplicate elements.

class Ex8
	def del_dup(l)
		dups, rights = Array.new, Array.new

		l.each do |i|
			if rights.include? i
				dups << i
			else
				rights << i
			end
		end

		dups
	end
end

require 'test/unit'

class TestEx8 < Test::Unit::TestCase
	def test_del_dup
		ex = Ex8.new
		assert_equal ex.del_dup(["1", "3", "5", "9", "1", "5", "9"]), ["1", "5", "9"]
	end
end