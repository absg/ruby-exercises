# A possible interview question from "Effective Programming More Than Writing Code".
# Write a function to print the odd numbers between 1 to 99.

def print_odd(x, y)
	(x..y).each do |i|
		puts i if (i % 2) > 0
	end
end

print_odd(1, 99)
