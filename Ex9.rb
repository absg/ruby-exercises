# A possible interview question from "Effective Programming More Than Writing Code".
# Write a program that receives as input a list of cars and returns a list containing the licenses of the correct cars avoiding duplicate models.
# A car is defined by its license number, brand, model and color. Two cars are equal if they have the same brand, model and color.

class Car
  attr_accessor :license, :brand, :model, :color

  def initialize(license, brand, model, color)
    @license, @brand, @model, @color = license, brand, model, color
  end

  def == (other)
	  if other.class == self.class
	    @brand == other.brand && @model == other.model && @color == other.color
	  else
	    false
	  end
	end
end

def del_dup(l)
	dups, rights = Array.new, Array.new

	l.each do |i|
		if rights.include? i
			dups << i
		else
			rights << i
		end
	end

	licenses = Array.new

	rights.each do |l|
		licenses << l.license
	end

	licenses
end

c1 = Car.new("73-23-MP", "Volkswagen", "Polo", "Blue")
c2 = Car.new("91-20-AW", "Volkswagen", "Polo", "Blue")
c3 = Car.new("44-09-EP", "Volkswagen", "Polo", "Red")
c4 = Car.new("55-10-ZA", "Volkswagen", "Golf", "Blue")

puts del_dup([c1, c2, c3, c4])